

const GreenEnergyBar = ( {green_level='73%', red_level='27%'} ) => {
  return (
    <div className='hidden md:flex flex-row h-16 p-4 align-middle w-full text-center'>
        <div className={`pt-1 bg-green-500/20 w-[73%] mb-2 rounded-lg text-xs text-green-400 invisible md:visible`}>{green_level} renewable</div>
        <div className={`pt-1 bg-red-500/20 w-[27%] mb-2 rounded-lg text-xs text-red-400 invisible md:visible`}>{red_level} non-renewable</div>
    </div>
  );
};

export default GreenEnergyBar;