import { BsPlus, BsFillLightningFill, BsGearFill } from 'react-icons/bs';
import { FaFire, FaPoo } from 'react-icons/fa';
import { TiWeatherStormy } from 'react-icons/ti';
import { BsQuestionLg, BsNewspaper } from 'react-icons/bs'
import { Outlet, NavLink, Link } from "react-router-dom";
import logo from '../assets/syj-logo.svg'

const NavBar = () => {
  return (
    <div className="top-0 left-0 h-screen min-w-[5rem] max-w-[15rem] w-1/5 flex flex-col border-gray-900 border-r
                  border-opacity-50 bg-white dark:bg-black shadow-lg overflow-hidden">

        <Logo />
        <Divider />
        <NavBarIcon icon={<BsFillLightningFill size="20" />} text={"Grid Live"} page='/gridlive' />
        <NavBarIcon icon={<TiWeatherStormy size="28"  />} text="Grid Forecast" page='/gridforecast'/>
        <NavBarIcon icon={<BsNewspaper size="20"  />} text="News" page='/news'/>
        <NavBarIcon icon={<BsQuestionLg size="24"  />} text="About" page='/about'/>
    </div>
  );
};

const NavBarIcon = ({ icon, text, page }) => (
    <NavLink to={page} className={({ isActive, isPending }) => isPending ? "pending" : isActive ? "bg-green-600 bg-opacity-80" : ""}>
      <div className="navbar-icon my-1 group content-center">
        {icon}
        <p className="text-white px-2 whitespace-nowrap hidden md:flex">{text}</p>
      </div>
    </NavLink>
);

const Logo = () => (
    <Link to='/'>
        <div className="pt-4 px-4">
            <img src={logo} className="w-auto min-w-[3rem]"></img>
        </div>
    </Link>
);


const Divider = () => <hr className="Navbar-hr pb-5 border-gray-900 border-opacity-50 max-w-40 mx-5" />;

export default NavBar;