import { Outlet, Link } from "react-router-dom";
import { useState } from 'react';
import GreenEnergyBar from './GreenEnergyBar'

const ContentContainer = () => {
  const randomNumber = Math.floor(Math.random() * 100)
  const remaining = 100 - randomNumber
  return (
    <div className='content-container dark:bg-black'>
        <GreenEnergyBar />
        <Outlet />
    </div>
  );
};

export default ContentContainer;