import React, { StrictMode } from 'react';
import { render } from 'react-dom';
import ReactDOM from 'react-dom/client';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import App from './App';
import Dashboard from './routes/Dashboard'
import GridLive from './routes/GridLive'
import GridForecast from './routes/GridForecast'
import News from './routes/News'
import About from './routes/About'
import ErrorPage from './error-page'
import './index.css';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
        {
            path: "/",
            element: <Dashboard />,
        },
        {
            path: "gridlive/",
            element: <GridLive />,
        },
        {
            path: "gridforecast/",
            element: <GridForecast />,
        },
        {
            path: "news/",
            element: <News />,
        },
        {
            path: "about/",
            element: <About />,
        },
    ]
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
