import { useState } from 'react';
import reactLogo from './assets/react.svg';
import './App.css';
import MainContent from './components/MainContent';
import NavBar from './components/NavBar';
import { Outlet, Link } from "react-router-dom";

function App() {
  return (
    <div className="flex dark">
        <NavBar />
        <MainContent />
    </div>
  )
}

export default App;
