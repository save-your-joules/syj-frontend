// import { useState } from 'react';

const GridLive = () => {
  return (
    <div className='content-container dark:bg-black pl-4 pt-4'>
        <p>
            Here's some (hopefully) useful grid data, displayed live.<br />
            Does a 30 minute delay count as live?<br /> Maybe.
        </p>
    </div>
  );
};

export default GridLive;