// import { useState } from 'react';

const Dashboard = () => {
  return (
    <div className='flex-column'>
        <div className='h-32 mb-4'>
            <h1 className='text-9xl text-center font-sans font-bold'>Save Your Joules</h1>
        </div>
        <div className='text-center'>
            <p>If you have many or if you have a few, we would all be better off, saving one or two.</p>
        </div>
    </div>
  );
};

export default Dashboard;